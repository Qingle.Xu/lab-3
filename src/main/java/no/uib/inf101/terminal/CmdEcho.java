package no.uib.inf101.terminal;

public class CmdEcho implements Command {

    @Override
    public String run(String[] args) {
        String temp = "";
        for(String s : args) {
            temp += s + " " ;
        }

        return temp;
    }

    @Override
    public String getName() {
        return "echo";
    }

}